import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import Navbar from '../components/Navbar'
import Card from '../components/Card'
import CardContanier from '../components/CardContanier'
import Select from '../components/Select'
import { useField } from '../hooks/useField'
import { useEffect,useState } from 'react'
export async function getServerSideProps() {
  // Get external data from the file system, API, DB, etc.
  const data = await fetch('http://localhost:1337/api/cakes?populate=*')
  const cakes = await data.json()

  // The value of the `props` key will be
  //  passed to the `Home` component
  return {
    props:{
      cakes
    }
  }
}
export default function Home({cakes}) {
  const [filteredCakes,setFilteredCakes] = useState(cakes)
  console.log(cakes)
  const size = useField('TAMANO')
  const topping = useField('TOPPING')
  const orderBy = useField('ORDENAR POR')
  useEffect(()=>{
    async function fetchCakesBySize(size){
      const data = await fetch(`http://localhost:1337/api/sizes?populate[cakes][populate][0]=size,topping&filters[size][$eq]=${size}`)
      const response = await data.json()
      const cakes = response?.data[0]?.attributes?.cakes
      console.log(cakes)
      setFilteredCakes(cakes)
    }
    if(size.value!=='TAMANO'){
      fetchCakesBySize(size.value)
    }
  },
  [size.value])

  useEffect(()=>{
    async function fetchCakesByTopping(topping){
      const data = await fetch(`http://localhost:1337/api/toppings?populate[cakes][populate][0]=size,topping&filters[topping_type][$eq]=${topping}`)
      const response = await data.json()
      const cakes = response?.data[0]?.attributes?.cakes
      console.log(cakes)
      setFilteredCakes(cakes)
    }
    if(topping.value!=='TOPPING'){
      fetchCakesByTopping(topping.value)
    }
  },
  [topping.value])

  useEffect(()=>{
    function orderCakes(order){
      if(order==='Mas Caro'){
        const data = [...filteredCakes.data].sort((a,b)=>b.attributes.price-a.attributes.price)
        setFilteredCakes({...cakes,data})
      }
      else if(order=='Mas Barato'){
        const data = [...filteredCakes.data].sort((a,b)=>a.attributes.price-b.attributes.price)
        setFilteredCakes({...cakes,data})
      }
      
    }
    if(topping.value!=='ORDENAR POR'){
      orderCakes(orderBy.value)
    }
  },
  [orderBy.value])

  return (
    <>
      <Select value={size.value} onChange={size.onChange} options={['Pequeño','Mediano','Grande']}/>
      <Select value={topping.value} onChange={topping.onChange} options={['Fondeau','Chantilly','Betun Italiano']}/>
      <Select value={orderBy.value} onChange={orderBy.onChange} options={['Mas Caro','Mas Barato']}/>
      <CardContanier cakes={filteredCakes}/>

    </>
  )
}
