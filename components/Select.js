import React from 'react'
import { useState } from 'react'

export default function Select({options,value,onChange}) {

    return (
			<select 
				className="form-select" 
				aria-label="Default select example"
				value={value}
				onChange={onChange}
				>
				<option value={value} hidden>{value}</option>
				{
					options.map((option,key)=>{
						return <option key={key} value={option}>{option}</option>
					})
				}
				
				
			</select>
    )
}
