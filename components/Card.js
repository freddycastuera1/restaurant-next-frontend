import React from 'react'

function Card({cake}) {
		const {name,imageURL,description,price,combo_price,topping,size} = cake.attributes
		const {data:{attributes:{topping_type}}} = topping
		const {data:{attributes:{size:size_type}}} = size

    return (
<div className="card mb-3" style={{maxWidth:'1400px'}} >
  <div className="row g-0">

    <div className="col-md-8 d-flex flex-column">
			<div className="card-header">
    		{name}
  		</div>
			<div className='d-flex'>
				<img src={imageURL} className="img-fluid rounded-start" alt="..." />
      	<div className="card-body">
        	<p className="card-text">{description}</p>
        	<p>Tamano: <span>{size_type}</span></p>
					<p>Topping: <span>{topping_type}</span></p>
      	</div>
			</div>
    </div>
		<div className='col-md-4'>
			<ul className="list-group d-flex flex-row  align-items-stretch h-100">
    		<li className="list-group-item w-50 d-flex flex-column h-100">
					<div className='text-center my-4'>PASTEL</div>
					<span className='text-center'>15-17 personas</span>
					<span className='text-center'>${price}</span>
					<span className='text-center'>*solo pastel</span>
					<button className='btn btn-success rounded-pill'>SELECCIONAR</button>
				</li>
    		<li className="list-group-item w-50 d-flex flex-column ">
					<div className='text-center my-4'>PAQUETE</div>
					<span className='text-center'>15-17 personas</span>
					<span className='text-center'>${combo_price}</span>
					<span className='text-center'>*solo pastel</span>
					<button className='btn btn-success rounded-pill'>SELECCIONAR</button>
				</li>
  		</ul>
		</div>
  </div>
</div>
    )
}

export default Card
