import React from 'react'
import Card from './Card'


export default function CardContanier({cakes}) {
    return (
       <ul className='container'>
           {cakes.data.map(item=>{
               return (<li key={item.id} style={{listStyleType:'none'}}>
                    <Card cake={item}/>
               </li>)
           })}
       </ul>
    )
}
